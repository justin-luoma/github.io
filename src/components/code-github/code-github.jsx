import React, {Component} from 'react';
import {
    Card,
    CardHeader,
    CardActions,
    CardText
} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';

import './code-github.css';

const style = {
    card: {
      backgroundColor: "#E8E8E8",
    },
};

class CodeGithubComponent extends Component {

    render() {
        return (
            <div className="code-github">
              <p className="headline">View the code for this page.</p>
                <Card style={style.card}>
                  <CardHeader
                    subtitle="Github Repository"
                    title="justin-luoma/justin-luoma.github.io"
                    avatar="data/github.png"
                  />
                <CardText>
                        This application is made with React and Material UI. Check out the code on Github.
                    </CardText>
                    <CardActions>
                        <FlatButton label="View on github" href="https://github.com/justin-luoma/justin-luoma.github.io" target="_blank"/>
                    </CardActions>
                </Card>
            </div>
        );
    }
}

export default CodeGithubComponent;
